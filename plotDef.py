import math
from bamboo.plots import Plot, EquidistantBinning, SummedPlot
from bamboo import treefunctions as op

from highlevelLambdas import *

# TODO : remove the self

########################   Channel title   #############################
def channelTitleLabel(channel):
    if (channel == "El"):
        channel = "e^{+}/e^{-}"
    elif (channel == "Mu"):
        channel ="#mu^{+}/#mu^{-}"
    channelLabel = {'labels': [{'text': channel, 'position': [0.23, 0.87], 'size': 36}]}
    return channelLabel 

######################  Plot Objects Number  ###########################
def objectsNumberPlot(channel,objName,suffix,objCont,sel,Nmax,xTitle):
    channelLabel = channelTitleLabel(channel)
    return Plot.make1D(channel+"_"+suffix+"_"+objName+"_N",
                       op.rng_len(objCont),
                       sel,
                       EquidistantBinning(Nmax,0.,Nmax),
                       xTitle = xTitle,
                       plotopts = channelLabel)

#######################   Channel Plot   ###############################
def channelPlot(sel,SunLepEl,SinlepMu,suffix,channel):
    """
    Plots the number of events in each channel
    """
    plots = []
    plots.append(objectsNumberPlot(channel = channel,
                                   suffix  = suffix,
                                   objName = "SinlepEl",
                                   objCont = SinlepEl,
                                   sel     = sel,
                                   Nmax    = 5,
                                   xTitle  = "N(e^{+}/e^{-})"))
    plots.append(objectsNumberPlot(channel = channel,
                                   suffix  = suffix,
                                   objName = "SinlepMu",
                                   objCont = SinlepMu,
                                   sel     = sel,
                                   Nmax    = 5,
                                   xTitle  = "N(#mu^{+}/#mu^{-})"))
    return plots

##########################  YIELD PLOT #################################
class makeYieldPlots:
    def __init__(self):
        self.calls = 0
        self.plots = []
    def addYield(self, sel, name, title):
        """
        Make Yield plot and use it also in the latex yield table
        sel     = refine selection
        name    = name of the PDF to be produced
        title   = title that will be used in the LateX yield table
        """
        self.plots.append(Plot.make1D("Yield_"+name,   
                                      op.c_int(0),
                                      sel,
                                      EquidistantBinning(1, 0., 1.),
                                      title = title + " Yield",
                                      xTitle = title + " Yield",
                                      plotopts = {"for-yields":True, "yields-title":title, 'yields-table-order':self.calls}))
        self.calls += 1

    def returnPlots(self):
        return self.plots

######################   TRIGGER PLOTS  ##############################
def triggerPlots(sel,triggerDict,suffix,channel):
    plots = []
    channelLabel = channelTitleLabel(channel)
    
    # Single Electron #
    plots.append(Plot.make1D("%s_%s_trigger_singleElectron"%(channel,suffix), 
                             op.OR(*triggerDict["SingleElectron"]),
                             sel, 
                             EquidistantBinning(2, 0., 2.), 
                             xTitle= "SingleElectron trigger",
                             plotopts = channelLabel))
    # Single Muon #
    plots.append(Plot.make1D("%s_%s_trigger_singleMuon"%(channel,suffix), 
                             op.OR(*triggerDict['SingleMuon']), 
                             sel, 
                             EquidistantBinning(2, 0., 2.), 
                             xTitle= "SingleMuon trigger",
                             plotopts = channelLabel))
    # Double Electron #
    plots.append(Plot.make1D("%s_%s_trigger_doubleElectron"%(channel,suffix), 
                             op.OR(*triggerDict['DoubleEGamma']), 
                             sel, 
                             EquidistantBinning(2, 0., 2.), 
                             xTitle= "DoubleEGamma trigger",
                             plotopts = channelLabel))
    # Double Muon #
    plots.append(Plot.make1D("%s_%s_trigger_doubleMuon"%(channel,suffix), 
                             op.OR(*triggerDict['DoubleMuon']), 
                             sel, 
                             EquidistantBinning(2, 0., 2.), 
                             xTitle= "DoubleMuon trigger",
                             plotopts = channelLabel))
    # Muon-Electron #
    plots.append(Plot.make1D("%s_%s_trigger_MuonElectron"%(channel,suffix), 
                             op.OR(*triggerDict['MuonEG']), 
                             sel, 
                             EquidistantBinning(2, 0., 2.), 
                             xTitle= "MuonEG trigger",
                             plotopts = channelLabel))

    return plots
##########################  MET PLOT #################################
def makeMETPlots(sel, met, suffix, channel):
    """
    Make MET basic plots
    sel         = refine selection 
    met         = MET object
    suffix      = string identifying the selecton 
    channel     = string identifying the channel of the dilepton (can be "NoChannel")
    """

    plots = []

    channelLabel = channelTitleLabel(channel)

    # PT plot #
    plots.append(Plot.make1D("%s_%s_met_pt"%(channel,suffix), 
                             met.pt, 
                             sel, 
                             EquidistantBinning(40, 0., 200.), 
                             title="Transverse momentum of the MET (channel %s)"%channel, 
                             xTitle= "P_{T}(MET) [GeV]",
                             plotopts = channelLabel))
    # Phi plot #
    plots.append(Plot.make1D("%s_%s_met_phi"%(channel,suffix), 
                             met.phi, 
                             sel, 
                             EquidistantBinning(20, -3.2, 3.2), 
                             title="Azimutal angle of the MET (channel %s)"%channel, 
                             xTitle= "#phi (MET)",
                             plotopts = channelLabel))

    return plots

##########################  DILEPTON PLOT #################################
def makeSinleptonPlots(sel, sinlepton, suffix, channel, is_MC=False):
    """
    Make dilepton basic plots
    sel         = refine selection 
    sinlepton   = sinlepton object (El, Mu)
    suffix      = string identifying the selecton 
    channel     = string identifying the channel of the sinlepton (can be "NoChannel")
    """
    plots = []

    channelLabel = channelTitleLabel(channel)

    # PT plot #
    plots.append(Plot.make1D("%s_%s_lepton_pt"%(channel,suffix), 
                             sinlepton[0].pt, 
                             sel, 
                             EquidistantBinning(60,0.,300.),
                             title="Transverse momentum of the lepton (channel %s)"%channel, 
                             xTitle= "P_{T} (lepton) [GeV]",
                             plotopts = channelLabel))

    # Eta plot #
    plots.append(Plot.make1D("%s_%s_lepton_eta"%(channel,suffix), 
                             sinlepton[0].eta, 
                             sel, 
                             EquidistantBinning(20, -3., 3.), 
                             title="Pseudorapidity of the lepton (channel %s)"%channel, 
                             xTitle= "#eta (lepton)",
                             plotopts = channelLabel))

    # PT-eta plots #
    plots.append(Plot.make2D("%s_%s_lepton_ptVSeta"%(channel,suffix), 
                             [sinlepton[0].pt, sinlepton[0].eta],
                             sel, 
                             [EquidistantBinning(60,0.,300.),EquidistantBinning(20, -3., 3.)],
                             xTitle= "P_{T} (lepton) [GeV]",
                             yTitle= "#eta (lepton)",
                             plotopts = channelLabel))

    # Phi plot #
    plots.append(Plot.make1D("%s_%s_lepton_phi"%(channel,suffix), 
                             sinlepton[0].phi, 
                             sel, 
                             EquidistantBinning(20, -3.2, 3.2), 
                             title="Azimutal angle of the lepton (channel %s)"%channel, 
                             xTitle= "#phi (lepton)",
                             plotopts = channelLabel))

    # GenPartFlav (if isMC) #
    plots.append(Plot.make1D("%s_%s_lepton_genPartFlav"%(channel,suffix), 
                             sinlepton[0].genPartFlav if is_MC else op.c_int(-1),
                             sel, 
                             EquidistantBinning(23, -1., 22.), 
                             title="Flavour of genParticle (channel %s)"%channel, 
                             xTitle= "GenParticle flavour (lepton)",
                             plotopts = channelLabel))

    return plots

########################## DELTA PLOTS #################################
def makeDeltaRPlots(sel,cont1,cont2,suffix,channel,isMC):
    plots = [] 

    channelLabel = channelTitleLabel(channel)

    mixedCont = op.combine((cont1,cont2))
    plots.append(Plot.make1D("%s_%s_DeltaR"%(channel,suffix), 
                             op.map(mixedCont,lambda c : op.deltaR(c[0].p4,c[1].p4)),
                             sel, 
                             EquidistantBinning(100,0.,5.), 
                             title="#Delta R %s"%channel, 
                             xTitle= "#Delta R",
                             plotopts = channelLabel))

#    mixedCont_inDeltaR_0p4 = op.combine((cont1,cont2),pred=lambda c1,c2 : op.deltaR(c1.p4,c2.p4)<0.4)
#    mixedCont_outDeltaR_0p4 = op.combine((cont1,cont2),pred=lambda c1,c2 : op.deltaR(c1.p4,c2.p4)>=0.4)
#    if isMC: # We don't have gen level info on data
#        plots.append(Plot.make1D("%s_%s_ID_DeltaRCut_Below0p4"%(channel,suffix), 
#                                 op.map(mixedCont_inDeltaR_0p4,lambda c : c[0].genPartFlav), # Print gen flavour of first cont (aka lepton)
#                                 sel, 
#                                 EquidistantBinning(22,0.,22.), 
#                                 title="%s genFlavour ID : #Delta R < 0.4"%channel, 
#                                 xTitle= "ID"))
#        plots.append(Plot.make1D("%s_%s_ID_DeltaRCut_Above0p4"%(channel,suffix), 
#                                 op.map(mixedCont_outDeltaR_0p4,lambda c : c[0].genPartFlav), # Print gen flavour of first cont (aka lepton)
#                                 sel, 
#                                 EquidistantBinning(22,0.,22.), 
#                                 title="%s genFlavour ID : #Delta R > 0.4"%channel, 
#                                 xTitle= "#ID"))
 
        # Electron/Muon  genPartFlav :    1       -> prompt electron
        #                                 3,4,5   -> light quark, c quark, b quark
        #                                 15      -> tau 
        #                                 22      -> photon conversion (only electron)
        # script : https://github.com/cms-nanoAOD/cmssw/blob/6f76b77b43c852475cff452a97203441a9c962d1/PhysicsTools/NanoAOD/plugins/CandMCMatchTableProducer.cc#L99-L138



    return plots

##########################  JETS SEPARATE PLOTS #################################
def makeAk4JetsPlots(sel, 
                      j1, j2, j3, j4,
                      has0b3j=False, has1b2j=False,
                      has2b1j=False, has2b2j=False,
                      has0b4j=False, has1b3j=False,
                      suffix, channel,
                      is_MC=False):

    plots = []

    channelLabel = channelTitleLabel(channel)

    if has0b3j or has0b4j:
        j1_base_name      = "%s_%s_leadjet_{var}"%(channel,suffix)
        j1_base_title     = "leading jet"
        j2_base_name      = "%s_%s_subleadjet_{var}"%(channel,suffix)
        j2_base_title     = "subleading jet"
        j3_base_name      = "%s_%s_subsubleadjet_{var}"%(channel,suffix)
        j3_base_title     = "subsubleading jet"
        #Di-Jet labels
        j1j2pT_name       = "%s_%s_PT_j1j2"%(channel,suffix)
        j1j2pT_title      = "PT_j1j2"
        j1j2dR_name       = "%s_%s_DeltaR_j1j2"%(channel,suffix)
        j1j2dR_title      = "dR_j1j2"
        j2j3dR_name       = "%s_%s_DeltaR_j2j3"%(channel,suffix)
        j2j3dR_title      = "dR_j2j3"
        j1j3dR_name       = "%s_%s_DeltaR_j1j3"%(channel,suffix)
        j1j3dR_title      = "dR_j1j3"
        j1j2dPhi_name     = "%s_%s_DeltaPhi_j1j2"%(channel,suffix)
        j1j2dPhi_title    = "dPhi_j1j2"
        j2j3dPhi_name     = "%s_%s_DeltaPhi_j2j3"%(channel,suffix)
        j2j3dPhi_title    = "dPhi_j2j3"
        j1j3dPhi_name     = "%s_%s_DeltaPhi_j1j3"%(channel,suffix)
        j1j3dPhi_title    = "dPhi_j1j3"
        #invM
        j1j2invM_name     = "%s_%s_InvM_j1j2"%(channel,suffix)
        j1j2invM_title    = "InvM_j1j2"
        if has0b4j: 
            j4_base_name  = "%s_%s_subsubsubleadjet_{var}"%(channel,suffix)
            j4_base_title = "subsubsubleading jet"
            #Di-Jet labels
            j3j3pT_name       = "%s_%s_PT_j3j4"%(channel,suffix)
            j3j3pT_title      = "PT_j3j4"
            j1j4dR_name   = "%s_%s_DeltaR_j1j4"%(channel,suffix)
            j1j4dR_title  = "dR_j1j4"
            j2j4dR_name   = "%s_%s_DeltaR_j2j4"%(channel,suffix)
            j2j4dR_title  = "dR_j2j4"
            j3j4dR_name   = "%s_%s_DeltaR_j3j4"%(channel,suffix)
            j3j4dR_title  = "dR_j3j4"
            j1j4dPhi_name   = "%s_%s_DeltaPhi_j1j4"%(channel,suffix)
            j1j4dPhi_title  = "dPhi_j1j4"
            j2j4dPhi_name   = "%s_%s_DeltaPhi_j2j4"%(channel,suffix)
            j2j4dPhi_title  = "dPhi_j2j4"
            j3j4dPhi_name   = "%s_%s_DeltaPhi_j3j4"%(channel,suffix)
            j3j4dPhi_title  = "dPhi_j3j4"
            #invM
            j3j4invM_name   = "%s_%s_InvM_j3j4"%(channel,suffix)
            j3j4invM_title  = "InvM_j3j4"
           

    elif has1b2j or has1b3j:
        j1_base_name      = "%s_%s_leadbjet_{var}"%(channel,suffix)
        j1_base_title     = "leading bjet"
        j2_base_name      = "%s_%s_leadjet_{var}"%(channel,suffix)
        j2_base_title     = "leading jet"
        j3_base_name      = "%s_%s_subleadjet_{var}"%(channel,suffix)
        j3_base_title     = "subleading jet"
        #Di-Jet labels
        j1j2pT_name       = "%s_%s_PT_b1j1"%(channel,suffix)
        j1j2pT_title      = "PT_b1j1"
        j1j2dR_name       = "%s_%s_DeltaR_b1j1"%(channel,suffix)
        j1j2dR_title      = "dR_b1j1"
        j2j3dR_name       = "%s_%s_DeltaR_j1j2"%(channel,suffix)
        j2j3dR_title      = "dR_j1j2"
        j1j3dR_name       = "%s_%s_DeltaR_b1j2"%(channel,suffix)
        j1j3dR_title      = "dR_b1j2"
        j1j2dPhi_name     = "%s_%s_DeltaPhi_b1j1"%(channel,suffix)
        j1j2dPhi_title    = "dPhi_b1j1"
        j2j3dPhi_name     = "%s_%s_DeltaPhi_j1j2"%(channel,suffix)
        j2j3dPhi_title    = "dPhi_j1j2"
        j1j3dPhi_name     = "%s_%s_DeltaPhi_b1j2"%(channel,suffix)
        j1j3dPhi_title    = "dPhi_b1j2"
        #invM
        j1j2invM_name     = "%s_%s_InvM_b1j1"%(channel,suffix)
        j1j2invM_title    = "InvM_b1j1"
        if has1b3j:
            j4_base_name  = "%s_%s_subsubleadjet_{var}"%(channel,suffix)
            j4_base_title = "subsubleading jet"
            #Di-Jet labels
            j3j4pT_name       = "%s_%s_PT_j2j3"%(channel,suffix)
            j3j4pT_title      = "PT_j2j3"
            j1j4dR_name   = "%s_%s_DeltaR_b1j3"%(channel,suffix)
            j1j4dR_title  = "dR_b1j3"
            j2j4dR_name   = "%s_%s_DeltaR_j1j3"%(channel,suffix)
            j2j4dR_title  = "dR_j1j3"
            j3j4dR_name   = "%s_%s_DeltaR_j2j3"%(channel,suffix)
            j3j4dR_title  = "dR_j2j3"
            j1j4dPhi_name   = "%s_%s_DeltaPhi_b1j3"%(channel,suffix)
            j1j4dPhi_title  = "dPhi_b1j3"
            j2j4dPhi_name   = "%s_%s_DeltaPhi_j1j3"%(channel,suffix)
            j2j4dPhi_title  = "dPhi_j1j3"
            j3j4dPhi_name   = "%s_%s_DeltaPhi_j2j3"%(channel,suffix)
            j3j4dPhi_title  = "dPhi_j2j3"
            #invM
            j3j4invM_name   = "%s_%s_InvM_j2j3"%(channel,suffix)
            j3j4invM_title  = "InvM_j2j3"

    elif has2b1j or has2b2j:
        j1_base_name      = "%s_%s_leadbjet_{var}"%(channel,suffix)
        j1_base_title     = "leading bjet"
        j2_base_name      = "%s_%s_subleadbjet_{var}"%(channel,suffix)
        j2_base_title     = "subleading bjet"
        j3_base_name      = "%s_%s_leadjet_{var}"%(channel,suffix)
        j3_base_title     = "leading jet"
        #Di-Jet labels
        j1j2pT_name       = "%s_%s_PT_b1b2"%(channel,suffix)
        j1j2pT_title      = "PT_b1b2"
        j1j2dR_name       = "%s_%s_DeltaR_b1b2"%(channel,suffix)
        j1j2dR_title      = "dR_b1b2"
        j2j3dR_name       = "%s_%s_DeltaR_b2j1"%(channel,suffix)
        j2j3dR_title      = "dR_b2j1"
        j1j3dR_name       = "%s_%s_DeltaR_b1j1"%(channel,suffix)
        j1j3dR_title      = "dR_b1j1"
        j1j2dPhi_name     = "%s_%s_DeltaPhi_b1b2"%(channel,suffix)
        j1j2dPhi_title    = "dPhi_b1b2"
        j2j3dPhi_name     = "%s_%s_DeltaPhi_b2j1"%(channel,suffix)
        j2j3dPhi_title    = "dPhi_b2j1"
        j1j3dPhi_name     = "%s_%s_DeltaPhi_b1j1"%(channel,suffix)
        j1j3dPhi_title    = "dPhi_b1j1"
        #invM
        j1j2invM_name     = "%s_%s_InvM_b1b2"%(channel,suffix)
        j1j2invM_title    = "InvM_b1b2"
        if has2b2j:
            j4_base_name      = "%s_%s_subleadjet_{var}"%(channel,suffix)
            j4_base_title     = "subleading jet"
            #Di-Jet labels
            j3j4pT_name       = "%s_%s_PT_j1j2"%(channel,suffix)
            j3j4pT_title      = "PT_j1j2"
            j1j4dR_name   = "%s_%s_DeltaR_b1j2"%(channel,suffix)
            j1j4dR_title  = "dR_b1j2"
            j2j4dR_name   = "%s_%s_DeltaR_b2j2"%(channel,suffix)
            j2j4dR_title  = "dR_b2j2"
            j3j4dR_name   = "%s_%s_DeltaR_j1j2"%(channel,suffix)
            j3j4dR_title  = "dR_j1j2"
            j1j4dPhi_name   = "%s_%s_DeltaPhi_b1j2"%(channel,suffix)
            j1j4dPhi_title  = "dPhi_b1j2"
            j2j4dPhi_name   = "%s_%s_DeltaPhi_b2j2"%(channel,suffix)
            j2j4dPhi_title  = "dPhi_b2j2"
            j3j4dPhi_name   = "%s_%s_DeltaPhi_j1j2"%(channel,suffix)
            j3j4dPhi_title  = "dPhi_j1j2"
            #invM
            j3j4invM_name   = "%s_%s_InvM_j1j2"%(channel,suffix)
            j3j4invM_title  = "InvM_j1j2"
            

    # leadjet plots #
    plots.append(Plot.make1D(j1_base_name.format(var="pt"),
                             j1.pt,
                             sel,
                             EquidistantBinning(80,0.,400.),
                             title='Transverse momentum of the %s'%j1_base_title,
                             xTitle="P_{T}(%s) [GeV]"%j1_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j1_base_name.format(var="eta"),
                             j1.eta,
                             sel,
                             EquidistantBinning(20,-3.,3.),
                             title='Pseudorapidity of the %s'%j1_base_title,
                             xTitle="#eta(%s)"%j1_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j1_base_name.format(var="phi"),
                             j1.phi,
                             sel,
                             EquidistantBinning(20,-3.2,3.2),
                             title='Azimutal angle of the %s'%j1_base_title,
                             xTitle="#phi(%s)"%j1_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j1_base_name.format(var="hadronflavour"),
                             op.abs(j1.hadronFlavour) if is_MC else op.c_int(-1),
                             sel,
                             EquidistantBinning(23,-1.,22.),
                             title='Hadron flavour of the %s'%j1_base_title,
                             xTitle="Hadron flavour (%s) [GeV]"%j1_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j1_base_name.format(var="partonflavour"),
                             op.abs(j2.partonFlavour) if is_MC else op.c_int(-1),
                             sel,
                             EquidistantBinning(23,-1.,22.),
                             title='Parton flavour of the %s'%j1_base_title,
                             xTitle="Parton flavour (%s) [GeV]"%j1_base_title,
                             plotopts = channelLabel))

    # subleadjet plots #
    plots.append(Plot.make1D(j2_base_name.format(var="pt"),
                             j2.pt,
                             sel,
                             EquidistantBinning(80,0.,400.),
                             title='Transverse momentum of the %s'%j2_base_title,
                             xTitle="P_{T}(%s) [GeV]"%j2_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j2_base_name.format(var="eta"),
                             j2.eta,
                             sel,
                             EquidistantBinning(20,-3.,3.),
                             title='Pseudorapidity of the %s'%j2_base_title,
                             xTitle="#eta(%s)"%j2_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j2_base_name.format(var="phi"),
                             j2.phi,
                             sel,
                             EquidistantBinning(20,-3.2,3.2),
                             title='Azimutal angle of the %s'%j2_base_title,
                             xTitle="#phi(%s)"%j2_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j2_base_name.format(var="hadronflavour"),
                             op.abs(j2.hadronFlavour) if is_MC else op.c_int(-1),
                             sel,
                             EquidistantBinning(23,-1.,22.),
                             title='Hadron flavour of the %s'%j2_base_title,
                             xTitle="Hadron flavour (%s) [GeV]"%j2_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j2_base_name.format(var="partonflavour"),
                             op.abs(j2.partonFlavour) if is_MC else op.c_int(-1),
                             sel,
                             EquidistantBinning(23,-1.,22.),
                             title='Parton flavour of the %s'%j2_base_title,
                             xTitle="Parton flavour (%s) [GeV]"%j2_base_title,
                             plotopts = channelLabel))
    
    # subsubleadjet plots #
    plots.append(Plot.make1D(j3_base_name.format(var="pt"),
                             j3.pt,
                             sel,
                             EquidistantBinning(80,0.,400.),
                             title='Transverse momentum of the %s'%j3_base_title,
                             xTitle="P_{T}(%s) [GeV]"%j3_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j3_base_name.format(var="eta"),
                             j3.eta,
                             sel,
                             EquidistantBinning(20,-3.,3.),
                             title='Pseudorapidity of the %s'%j3_base_title,
                             xTitle="#eta(%s)"%j3_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j3_base_name.format(var="phi"),
                             j3.phi,
                             sel,
                             EquidistantBinning(20,-3.2,3.2),
                             title='Azimutal angle of the %s'%j3_base_title,
                             xTitle="#phi(%s)"%j3_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j3_base_name.format(var="hadronflavour"),
                             op.abs(j3.hadronFlavour) if is_MC else op.c_int(-1),
                             sel,
                             EquidistantBinning(23,-1.,22.),
                             title='Hadron flavour of the %s'%j3_base_title,
                             xTitle="Hadron flavour (%s) [GeV]"%j3_base_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j3_base_name.format(var="partonflavour"),
                             op.abs(j2.partonFlavour) if is_MC else op.c_int(-1),
                             sel,
                             EquidistantBinning(23,-1.,22.),
                             title='Parton flavour of the %s'%j3_base_title,
                             xTitle="Parton flavour (%s) [GeV]"%j3_base_title,
                             plotopts = channelLabel))

    if has0b4j or has1b3j or has2b2j:
        # subsubsubleadjet plots #
        plots.append(Plot.make1D(j4_base_name.format(var="pt"),
                                 j4.pt,
                                 sel,
                                 EquidistantBinning(80,0.,400.),
                                 title='Transverse momentum of the %s'%j4_base_title,
                                 xTitle="P_{T}(%s) [GeV]"%j4_base_title,
                                 plotopts = channelLabel))
        plots.append(Plot.make1D(j4_base_name.format(var="eta"),
                                 j4.eta,
                                 sel,
                                 EquidistantBinning(20,-3.,3.),
                                 title='Pseudorapidity of the %s'%j4_base_title,
                                 xTitle="#eta(%s)"%j4_base_title,
                                 plotopts = channelLabel))
        plots.append(Plot.make1D(j4_base_name.format(var="phi"),
                                 j4.phi,
                                 sel,
                                 EquidistantBinning(20,-3.2,3.2),
                                 title='Azimutal angle of the %s'%j4_base_title,
                                 xTitle="#phi(%s)"%j4_base_title,
                                 plotopts = channelLabel))
        plots.append(Plot.make1D(j4_base_name.format(var="hadronflavour"),
                                 op.abs(j4.hadronFlavour) if is_MC else op.c_int(-1),
                                 sel,
                                 EquidistantBinning(23,-1.,22.),
                                 title='Hadron flavour of the %s'%j4_base_title,
                                 xTitle="Hadron flavour (%s) [GeV]"%j4_base_title,
                                 plotopts = channelLabel))
        plots.append(Plot.make1D(j4_base_name.format(var="partonflavour"),
                                 op.abs(j2.partonFlavour) if is_MC else op.c_int(-1),
                                 sel,
                                 EquidistantBinning(23,-1.,22.),
                                 title='Parton flavour of the %s'%j4_base_title,
                                 xTitle="Parton flavour (%s) [GeV]"%j4_base_title,
                                 plotopts = channelLabel))

    # Dijet Pt plot #
    plots.append(Plot.make1D(j1j2pT_name, 
                             (j1.p4+j2.p4).Pt(), 
                             sel, 
                             EquidistantBinning(80,0.,400.),
                             title="Transverse momentum of the dijet_12 (channel %s)"%channel, 
                             xTitle= j1j2pT_title,
                             plotopts = channelLabel))
    if has0b4j or has1b3j or has2b2j:
        plots.append(Plot.make1D(j3j4pT_name, 
                                 (j3.p4+j4.p4).Pt(), 
                                 sel, 
                                 EquidistantBinning(80,0.,400.),
                                 title="Transverse momentum of the dijet_34 (channel %s)"%channel, 
                                 xTitle= j3j4pT_title,
                                 plotopts = channelLabel))
        

    # DeltaPhi plot #
    plots.append(Plot.make1D(j1j2dPhi_name, 
                             op.abs(op.deltaPhi(j1.p4,j2.p4)), 
                             sel, 
                             EquidistantBinning(20,0, 3.2),
                             title="Azimutal angle difference of the dijet_12 (channel %s)"%channel,
                             xTitle= j1j2dPhi_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j1j3dPhi_name, 
                             op.abs(op.deltaPhi(j1.p4,j3.p4)), 
                             sel, 
                             EquidistantBinning(20,0, 3.2),
                             title="Azimutal angle difference of the dijet_13 (channel %s)"%channel,
                             xTitle= j1j3dPhi_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j2j3dPhi_name, 
                             op.abs(op.deltaPhi(j2.p4,j3.p4)), 
                             sel, 
                             EquidistantBinning(20,0, 3.2),
                             title="Azimutal angle difference of the dijet_12 (channel %s)"%channel,
                             xTitle= j2j3dPhi_title,
                             plotopts = channelLabel))
    if has0b4j or has1b3j or has2b2j:
        plots.append(Plot.make1D(j1j4dPhi_name, 
                                 op.abs(op.deltaPhi(j1.p4,j4.p4)), 
                                 sel, 
                                 EquidistantBinning(20,0, 3.2),
                                 title="Azimutal angle difference of the dijet_14 (channel %s)"%channel,
                                 xTitle= j1j4dPhi_title,
                                 plotopts = channelLabel))
        plots.append(Plot.make1D(j2j4dPhi_name, 
                                 op.abs(op.deltaPhi(j2.p4,j4.p4)), 
                                 sel, 
                                 EquidistantBinning(20,0, 3.2),
                                 title="Azimutal angle difference of the dijet_24 (channel %s)"%channel,
                                 xTitle= j2j4dPhi_title,
                                 plotopts = channelLabel))
        plots.append(Plot.make1D(j3j4dPhi_name, 
                                 op.abs(op.deltaPhi(j3.p4,j4.p4)), 
                                 sel, 
                                 EquidistantBinning(20,0, 3.2),
                                 title="Azimutal angle difference of the dijet_34 (channel %s)"%channel,
                                 xTitle= j3j4dPhi_title,
                                 plotopts = channelLabel))

    # DeltaR plot #
    plots.append(Plot.make1D(j1j2dR_name, 
                             op.deltaR(j1.p4,j2.p4)), 
                             sel, 
                             EquidistantBinning(50,0, 5.0),
                             title="dijet_12 DeltaR(channel %s)"%channel,
                             xTitle= j1j2dR_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j1j3dR_name, 
                             op.deltaR(j1.p4,j3.p4)), 
                             sel, 
                             EquidistantBinning(50,0, 5.0),
                             title="dijet_13 DeltaR(channel %s)"%channel,
                             xTitle= j1j3dR_title,
                             plotopts = channelLabel))
    plots.append(Plot.make1D(j2j3dR_name, 
                             op.deltaR(j2.p4,j3.p4)), 
                             sel, 
                             EquidistantBinning(50,0, 5.0),
                             title="dijet_23 DeltaR (channel %s)"%channel,
                             xTitle= j2j3dR_title,
                             plotopts = channelLabel))
    if has0b4j or has1b3j or has2b2j:
        plots.append(Plot.make1D(j1j4dR_name, 
                                 op.deltaR(j1.p4,j4.p4)), 
                                 sel, 
                                 EquidistantBinning(50,0, 5.0),
                                 title="dijet_14 DeltaR (channel %s)"%channel,
                                 xTitle= j1j4dR_title,
                                 plotopts = channelLabel))
        plots.append(Plot.make1D(j2j4dR_name, 
                                 op.deltaPhi(j2.p4,j4.p4)), 
                                 sel, 
                                 EquidistantBinning(50,0, 5.0),
                                 title="dijet_24 DeltaR (channel %s)"%channel,
                                 xTitle= j2j4dR_title,
                                 plotopts = channelLabel))
        plots.append(Plot.make1D(j3j4dPhi_name, 
                                 op.deltaPhi(j3.p4,j4.p4)), 
                                 sel, 
                                 EquidistantBinning(50,0, 5.0),
                                 title="dijet_34 DeltaR (channel %s)"%channel,
                                 xTitle= j3j4dR_title,
                                 plotopts = channelLabel))
        

    # invariant mass plot #
    plots.append(Plot.make1D(j1j2invM_name,
                             op.invariant_mass(j1.p4,j2.p4),
                             sel,
                             EquidistantBinning(50, 0., 500.), 
                             title="Dijet_12 invariant mass (channel %s)"%channel, 
                             xTitle= j1j2invM_title,
                             plotopts = channelLabel))
    if has0b4j or has1b3j or has2b2j:
        plots.append(Plot.make1D(j3j4invM_name,
                                 op.invariant_mass(j3.p4,j4.p4),
                                 sel,
                                 EquidistantBinning(50, 0., 500.), 
                                 title="Dijet_34 invariant mass (channel %s)"%channel, 
                                 xTitle= j3j4invM_title,
                                 plotopts = channelLabel))


    return plots

##########################  JETS (AK8) PLOT #################################
def makeAk8JetsPlots(sel, fatjet, suffix, channel):
    """
    Make fatjet subjet basic plots
    sel         = refine selection 
    fatjet      = fatjet (AK8 jet) 
    suffix      = string identifying the selecton 
    channel     = string identifying the channel of the dilepton (can be "NoChannel")
    """
 
    plots = []

    channelLabel = channelTitleLabel(channel)

    # fatjet plots (always present by selection) #
    plots.append(Plot.make1D("%s_%s_fatjet_pt"%(channel,suffix),
                             fatjet.p4.pt(),
                             sel,
                             EquidistantBinning(60,0.,600.),
                             title='Transverse momentum of the fatjet',
                             xTitle="P_{T}(fatjet) [GeV]",
                             plotopts = channelLabel))
    plots.append(Plot.make1D("%s_%s_fatjet_eta"%(channel,suffix),
                             fatjet.p4.eta(),
                             sel,
                             EquidistantBinning(10,-3.,3.),
                             title='Pseudorapidity of the fatjet',
                             xTitle="#eta(fatjet)",
                             plotopts = channelLabel))
    plots.append(Plot.make1D("%s_%s_fatjet_phi"%(channel,suffix),
                             fatjet.p4.phi(),
                             sel,
                             EquidistantBinning(10,-3.2,3.2),
                             title='Azimutal angle of the fatjet',
                             xTitle="#phi(fatjet)",
                             plotopts = channelLabel))
    plots.append(Plot.make1D("%s_%s_fatjet_mass"%(channel,suffix),
                             fatjet.mass,
                             sel,
                             EquidistantBinning(40,0.,200.),
                             title='Invariant mass of the fatjet',
                             xTitle="M(fatjet)",
                             plotopts = channelLabel))
    plots.append(Plot.make1D("%s_%s_fatjet_softdropmass"%(channel,suffix),
                             fatjet.msoftdrop,
                             sel,
                             EquidistantBinning(40,0.,200.),
                             title='Soft Drop mass of the fatjet',
                             xTitle="M_{Soft Drop}(fatjet) [GeV]",
                             plotopts = channelLabel))

    return plots

#########################  High-level quantities ################################
def makeHighLevelQuantities(sel,met,lep,
                            j1,j2,j3,j4=None,
                            has0b3j=False, has1b2j=False, has2b1j=False,
                            has0b4j=False, has1b3j=False, has2b2j=False,
                            suffix,channel):
    if has0b3j or has0b4j:
        #Di-Jet labels
        lepj1dR_name       = "%s_%s_DeltaR_lepj1"%(channel,suffix)
        lepj1dR_title      = "dR_lepj1"
        lepj2dR_name       = "%s_%s_DeltaR_lepj2"%(channel,suffix)
        lepj2dR_title      = "dR_lepj2"
        lepj3dR_name       = "%s_%s_DeltaR_lepj3"%(channel,suffix)
        lepj3dR_title      = "dR_lepj3"
        lepj1dPhi_name     = "%s_%s_DeltaPhi_lepj1"%(channel,suffix)
        lepj1dPhi_title    = "dPhi_lepj1"
        lepj2dPhi_name     = "%s_%s_DeltaPhi_lepj2"%(channel,suffix)
        lepj2dPhi_title    = "dPhi_lepj2"
        lepj3dPhi_name     = "%s_%s_DeltaPhi_lepj3"%(channel,suffix)
        lepj3dPhi_title    = "dPhi_lepj3"
        if has0b4j: 
            lepj4dR_name     = "%s_%s_DeltaR_lepj4"%(channel,suffix)
            lepj4dR_title    = "dR_j1j4"
            lepj4dPhi_name   = "%s_%s_DeltaPhi_lepj4"%(channel,suffix)
            lepj4dPhi_title  = "dPhi_j1j4"

    if has1b2j or has1b3j:
        #Di-Jet labels
        lepj1dR_name       = "%s_%s_DeltaR_lepb1"%(channel,suffix)
        lepj1dR_title      = "dR_lepb1"
        lepj2dR_name       = "%s_%s_DeltaR_lepj1"%(channel,suffix)
        lepj2dR_title      = "dR_lepj1"
        lepj3dR_name       = "%s_%s_DeltaR_lepj2"%(channel,suffix)
        lepj3dR_title      = "dR_lepj2"
        lepj1dPhi_name     = "%s_%s_DeltaPhi_lepb1"%(channel,suffix)
        lepj1dPhi_title    = "dPhi_lepb1"
        lepj2dPhi_name     = "%s_%s_DeltaPhi_lepj1"%(channel,suffix)
        lepj2dPhi_title    = "dPhi_lepj1"
        lepj3dPhi_name     = "%s_%s_DeltaPhi_lepj2"%(channel,suffix)
        lepj3dPhi_title    = "dPhi_lepj2"
        if has1b3j: 
            lepj4dR_name     = "%s_%s_DeltaR_lepj3"%(channel,suffix)
            lepj4dR_title    = "dR_lepj3"
            lepj4dPhi_name   = "%s_%s_DeltaPhi_lepj3"%(channel,suffix)
            lepj4dPhi_title  = "dPhi_lepj3"

    if has2b1j or has2b2j:
        #Di-Jet labels
        lepj1dR_name       = "%s_%s_DeltaR_lepb1"%(channel,suffix)
        lepj1dR_title      = "dR_lepb1"
        lepj2dR_name       = "%s_%s_DeltaR_lepb2"%(channel,suffix)
        lepj2dR_title      = "dR_lepb2"
        lepj3dR_name       = "%s_%s_DeltaR_lepj1"%(channel,suffix)
        lepj3dR_title      = "dR_lepj1"
        lepj1dPhi_name     = "%s_%s_DeltaPhi_lepb1"%(channel,suffix)
        lepj1dPhi_title    = "dPhi_lepb1"
        lepj2dPhi_name     = "%s_%s_DeltaPhi_lepb1"%(channel,suffix)
        lepj2dPhi_title    = "dPhi_lepb1"
        lepj3dPhi_name     = "%s_%s_DeltaPhi_lepj1"%(channel,suffix)
        lepj3dPhi_title    = "dPhi_lepj1"
        if has1b3j: 
            lepj4dR_name     = "%s_%s_DeltaR_lepj2"%(channel,suffix)
            lepj4dR_title    = "dR_lepj2"
            lepj4dPhi_name   = "%s_%s_DeltaPhi_lepj2"%(channel,suffix)
            lepj4dPhi_title  = "dPhi_lepj2"

    plots = []

    channelLabel = channelTitleLabel(channel)

    # lepton_Jet DeltaR plots #
    plots.append(Plot.make1D(lepj1dR_name,
                             op.deltaR(lep.p4,j1.p4),
                             sel,
                             EquidistantBinning(50,0.,5.),
                             title="lep_jet1 DeltaR(channel %s)"%channel,
                             xTitle=lepj1dR_title,
                             plotopts=channelLabel))
    plots.append(Plot.make1D(lepj2dR_name,
                             op.deltaR(lep.p4,j2.p4),
                             sel,
                             EquidistantBinning(50,0.,5.),
                             title="lep_jet2 DeltaR(channel %s)"%channel,
                             xTitle=lepj2dR_title,
                             plotopts=channelLabel))
    plots.append(Plot.make1D(lepj3dR_name,
                             op.deltaR(lep.p4,j3.p4),
                             sel,
                             EquidistantBinning(50,0.,5.),
                             title="lep_jet3 DeltaR(channel %s)"%channel,
                             xTitle=lepj3dR_title,
                             plotopts=channelLabel))
    if has0b4j or has1b3j or has2b2j:
        plots.append(Plot.make1D(lepj4dR_name,
                                 op.deltaR(lep.p4,j4.p4),
                                 sel,
                                 EquidistantBinning(50,0.,5.),
                                 title="lep_jet4 DeltaR(channel %s)"%channel,
                                 xTitle=lepj4dR_title,
                                 plotopts=channelLabel))


    # lepton_Jet DeltaPhi plots #
    plots.append(Plot.make1D(lepj1dPhi_name,
                             op.abs(op.deltaPhi(lep.p4,j1.p4)),
                             sel,
                             EquidistantBinning(20,0.,3.2),
                             title="lep_jet1 DeltaPhi(channel %s)"%channel,
                             xTitle=lepj1dPhi_title,
                             plotopts=channelLabel))
    plots.append(Plot.make1D(lepj2dPhi_name,
                             op.abs(op.deltaPji(lep.p4,j2.p4)),
                             sel,
                             EquidistantBinning(20,0.,3.2),
                             title="lep_jet2 DeltaPhi(channel %s)"%channel,
                             xTitle=lepj2dPhi_title,
                             plotopts=channelLabel))
    plots.append(Plot.make1D(lepj3dPhi_name,
                             op.deltaPhi(lep.p4,j3.p4),
                             sel,
                             EquidistantBinning(20,0.,3.2),
                             title="lep_jet3 DeltaPhi(channel %s)"%channel,
                             xTitle=lepj3dPhi_title,
                             plotopts=channelLabel))
    if has0b4j or has1b3j or has2b2j:
        plots.append(Plot.make1D(lepj4dPhi_name,
                                 op.abs(op.deltaPhi(lep.p4,j4.p4)),
                                 sel,
                                 EquidistantBinning(20,0.,3.2),
                                 title="lep_jet4 DeltaPhi(channel %s)"%channel,
                                 xTitle=lepj4dPhi_title,
                                 plotopts=channelLabel))

    # lepton-MET plots #
    plots.append(Plot.make1D("%s_%s_highlevelvariable_SinglepMET_Pt"%(channel,suffix),
                             op.abs(SinglepMet_Pt(lep, met)),
                             sel,
                             EquidistantBinning(20,0.,3.2),
                             title='Reultant pT of lepton and MET (%s channel)'%channel,
                             xTitle="|Pt (lep,MET)|",
                             plotopts = channelLabel))
    plots.append(Plot.make1D("%s_%s_highlevelvariable_SinglepMETdeltaPhi"%(channel,suffix),
                             op.abs(SinglepMet_dPhi(lep, met)),
                             sel,
                             EquidistantBinning(20,0.,3.2),
                             title='Azimutal angle between lepton and MET (%s channel)'%channel,
                             xTitle="|#Delta \phi (lep,MET)|",
                             plotopts = channelLabel))

    # Transverse mass plots #
    # mT for Single lepton #
    plots.append(Plot.make1D("%s_%s_highlevelvariable_MT"%(channel,suffix),
                             MT(lep,met),
                             sel,
                             EquidistantBinning(50,0.,1000.),
                             title='Transverse mass of lepton and MET (%s channel)'%channel,
                             xTitle="M_{T}(lep,MET) [GeV]",
                             plotopts = channelLabel))
    # mT for ljj #
    if has1b2j or has1b3j:
        plots.append(Plot.make1D("%s_%s_highlevelvariable_MTlepjj"%(channel,suffix),
                                 MT_W1W2_ljj(lep,j2,j3,met),
                                 sel,
                                 EquidistantBinning(50,0.,1000.),
                                 title='Transverse mass of lepton+dijet and MET (%s channel)'%channel,
                                 xTitle="M_{T}(lepjj,MET) [GeV]",
                                 plotopts = channelLabel))
    elif has2b2j:
        plots.append(Plot.make1D("%s_%s_highlevelvariable_MTlepjj"%(channel,suffix),
                                 MT_W1W2_ljj(lep,j3,j4,met),
                                 sel,
                                 EquidistantBinning(50,0.,1000.),
                                 title='Transverse mass of lepton+dijet and MET (%s channel)'%channel,
                                 xTitle="M_{T}(lepjj,MET) [GeV]",
                                 plotopts = channelLabel))
    elif has2b1j:
        plots.append(Plot.make1D("%s_%s_highlevelvariable_MTlepj"%(channel,suffix),
                                 MT_W1W2_lj(lep,j3,met),
                                 sel,
                                 EquidistantBinning(50,0.,1000.),
                                 title='Transverse mass of lepton+jet and MET (%s channel)'%channel,
                                 xTitle="M_{T}(lepj,MET) [GeV]",
                                 plotopts = channelLabel))            

    if has1b2j or has2b1j:
        # Scalar magnitude sum #
        plots.append(Plot.make1D("%s_%s_highlevelvariable_HT2_l3jmet"%(channel,suffix),
                                 HT2_l3jmet(lep,j1,j2,j3,met),
                                 sel,
                                 EquidistantBinning(60,0.,600.),
                                 title='Di-Higgs magnitude_l3jmet (%s channel)'%channel,
                                 xTitle="H_{T2} [GeV]",
                                 plotopts = channelLabel))
        plots.append(Plot.make1D("%s_%s_highlevelvariable_HT2R_l3jmet"%(channel,suffix),
                                 HT2R_l3jmet(lep,j1,j2,j3,met),
                                 sel,
                                 EquidistantBinning(60,0.,600.),
                                 title='Di-Higgs magnitude_Ratio_l3jmet (%s channel)'%channel,
                                 xTitle="H_{T2}_Ratio [GeV]",
                                 plotopts = channelLabel))
    elif has2b2j:
        plots.append(Plot.make1D("%s_%s_highlevelvariable_HT2_l4jmet"%(channel,suffix),
                                 HT2_l4jmet(lep,j1,j2,j3,j4,met),
                                 sel,
                                 EquidistantBinning(60,0.,600.),
                                 title='Di-Higgs magnitude_l4jmet (%s channel)'%channel,
                                 xTitle="H_{T2} [GeV]",
                                 plotopts = channelLabel))
        plots.append(Plot.make1D("%s_%s_highlevelvariable_HT2R_l4jmet"%(channel,suffix),
                                 HT2R_l4jmet(lep,j1,j2,j3,j4,met),
                                 sel,
                                 EquidistantBinning(60,0.,600.),
                                 title='Di-Higgs magnitude_Ratio_l4jmet (%s channel)'%channel,
                                 xTitle="H_{T2}_Ratio [GeV]",
                                 plotopts = channelLabel))

    return plots 

