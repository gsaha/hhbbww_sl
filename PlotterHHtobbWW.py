import os
import sys
from copy import copy

from bamboo.analysismodules import HistogramsModule
from bamboo import treefunctions as op
from bamboo.plots import Plot, EquidistantBinning, SummedPlot

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)))) # Add scripts in this directory
from BaseHHtobbWW import BaseNanoHHtobbWW
from plotDef import *
from selectionDef import *


#===============================================================================================#
#                                       PlotterHHtobbWW                                         #
#===============================================================================================#
class PlotterNanoHHtobbWW(BaseNanoHHtobbWW,HistogramsModule):
    """ Plotter module: HH->bbW(->e/µ nu)W(->e/µ nu) histograms from NanoAOD """
    def __init__(self, args):
        super(PlotterNanoHHtobbWW, self).__init__(args)

    def definePlots(self, t, noSel, sample=None, sampleCfg=None): 
        noSel = super(PlotterNanoHHtobbWW,self).prepareObjects(t, noSel, sample, sampleCfg)
        plots = []
        era = sampleCfg['era']
        self.yieldPlots = makeYieldPlots()
        #----- Dileptons -----#
        selObjectDict = makeLeptonSelection(self,noSel,plot_yield=True)
        # selObjectDict : keys -> level (str)
        #                 values -> [ElEl,MuMu,ElMu] x Selection object
        # Select the jets selections that will be done depending on user input #
        jet_level = ["Ak4","Ak8","LooseResolved0b3j","LooseResolved1b2j","LooseResolved2b1j","LooseResolved2b2j",
                     "TightResolved0b4j","TightResolved1b3j","TightResolved2b2j",
                     "SemiBoostedHfatWslim","SemiBoostedHslimWfat",
                     "Boosted"]
        jetplot_level = [arg for (arg,boolean) in self.args.__dict__.items() if arg in jet_level and boolean]
        if len(jetplot_level) == 0:  
            jetplot_level = jet_level # If nothing said, will do all
        jetsel_level = copy(jetplot_level)  # A plot level might need a previous selection that needs to be defined but not necessarily plotted
        if any("Resolved" in key for key in jetsel_level):     
            jetsel_level.append("Ak4") # Resolved needs the Ak4 selection
        if any("SemiBoosted" in key for key in jetsel_level):     
            jetsel_level.append("Ak4") # Resolved needs the Ak4 selection
            jetsel_level.append("Ak8") # Boosted needs the Ak8 selection
        if "Boosted" in jetsel_level:
            jetsel_level.append("Ak8") # Boosted needs the Ak8 selection

        # Selections:    
        # Loop over lepton selection and start plotting #
        for selectionType, selectionList in selObjectDict.items():
            print ("... Processing %s lepton type"%selectionType)
            #----- Select correct dilepton -----#
            if selectionType == "Preselected":  
                ElColl = self.OSElElDileptonPreSel
                MuColl = self.OSMuMuDileptonPreSel
            elif selectionType == "Fakeable":
                ElColl = self.OSElElDileptonFakeSel
                MuColl = self.OSMuMuDileptonFakeSel
            elif selectionType == "Tight":
                ElColl = self.OSElElDileptonTightSel
                MuColl = self.OSMuMuDileptonTightSel
            elif selectionType == "FakeExtrapolation":
                ElColl = self.OSElElDileptonFakeExtrapolationSel
                MuColl = self.OSMuMuDileptonFakeExtrapolationSel

            #----- Separate selections ------#
            ElSelObj = selectionList[0]
            MuSelObj = selectionList[1]

            if not self.args.OnlyYield:
                #----- Trigger plots -----#
                plots.extend(triggerPlots(sel = ElSelObj.sel,triggerDict = self.triggersPerPrimaryDataset,suffix = ElSelObj.selName,channel = "El"))
                plots.extend(triggerPlots(sel = MuSelObj.sel,triggerDict = self.triggersPerPrimaryDataset,suffix = MuSelObj.selName,channel = "Mu"))

                #----- Lepton plots -----#
                # Singlelepton channel plots #
                plots.extend(channelPlot(sel = ElSelObj.sel,SinlepEl = ElColl,SinlepMu = MuColl,suffix = ElSelObj.selName,channel = "El"))
                plots.extend(channelPlot(sel = MuSelObj.sel,SinlepEl = ElColl,SinlepMu = MuColl,suffix = MuSelObj.selName,channel = "Mu"))
        
            #----- Ak4 jets selection -----#
            LeptonKeys  = ['channel','sel','sinlepton','suffix','is_MC']
            JetKeys     = ['channel','sel','j1','j2','j3','j4':None,'has0b3j','has1b2j','has2b1j','has0b4j','has1b3j','has2b2j','suffix','is_MC']
            commonItems = ['channel','sel','suffix']
            if "Ak4" in jetsel_level:
                print("... Processing LooseResolvedAk4JetsSelection")
                if any("LooseResolved" in key for key in jetsel_level):
                    print ("...... Processing Ak4 jet selection Loose (nAk4Jets >= 3)")
                    ElSelObjAk4JetsLoose = makeAk4JetSelection(self,ElSelObj,3,copy_sel=True,plot_yield=True)
                    MuSelObjAk4JetsLoose = makeAk4JetSelection(self,MuSelObj,3,copy_sel=True,plot_yield=True)

                    # Jet and lepton plots #
                    ChannelDictList = []
                    if not self.args.OnlyYield and "Ak4" in jetplot_level:
                        ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsLoose.sel,'sinlepton':ElColl[0],
                                               'j1':self.ak4Jets[0],'j2':self.ak4Jets[1],'j3':self.ak4Jets[2],
                                                'has0b3j':True,
                                                'suffix':ElSelObjAk4JetsLoose.selName,
                                                'is_MC':self.is_MC})
                        ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsLoose.sel,'sinlepton':MuColl[0],
                                                'j1':self.ak4Jets[0],'j2':self.ak4Jets[1],'j3':self.ak4Jets[2],
                                                'has0b3j':True,
                                                'suffix':MuSelObjAk4JetsLoose.selName,
                                                'is_MC':self.is_MC})

                    JetsN = {'objName':'Ak4Jets','objCont':self.ak4Jets,'Nmax':10,'xTitle':'N(Ak4 jets)'}
                                                            
                    for channelDict in ChannelDictList:
                        # Dilepton #
                        plots.extend(makeSinleptonPlots(**{k:channelDict[k] for k in LeptonKeys}))
                        # Number of jets #
                        plots.append(objectsNumberPlot(**{k:channelDict[k] for k in commonItems},**JetsN))
                        # Ak4 Jets #
                        plots.extend(makeAk4JetsPlots(**{k:channelDict[k] for k in JetKeys}))
                        # MET #
                        plots.extend(makeMETPlots(**{k:channelDict[k] for k in commonItems}, met=self.corrMET))


                print("... Processing TightResolvedAk4JetsSelection")
                if any("TightResolved" in key for key in jetsel_level):        
                    print ("...... Processing Ak4 jet selection Tight (nAk4Jets >= 4)")
                    ElSelObjAk4JetsTight = makeAk4JetSelection(self,ElSelObj,4,copy_sel=True,plot_yield=True)
                    MuSelObjAk4JetsTight = makeAk4JetSelection(self,MuSelObj,4,copy_sel=True,plot_yield=True)

                    # Jet and lepton plots #
                    ChannelDictList = []
                    if not self.args.OnlyYield and "Ak4" in jetplot_level:
                    ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsTight.sel,'sinlepton':ElColl[0],
                                            'j1':self.ak4Jets[0],'j2':self.ak4Jets[1],'j3':self.ak4Jets[2],'j4':self.ak4Jets[3],
                                            'has0b4j':True,
                                            'suffix':ElSelObjAk4JetsTight.selName,
                                            'is_MC':self.is_MC})
                    ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsTight.sel,'sinlepton':MuColl[0],
                                            'j1':self.ak4Jets[0],'j2':self.ak4Jets[1],'j3':self.ak4Jets[2],'j4':self.ak4Jets[3],
                                            'has0b4j':True,
                                            'suffix':MuSelObjAk4JetsTight.selName,
                                            'is_MC':self.is_MC})

                    JetsN = {'objName':'Ak4Jets','objCont':self.ak4Jets,'Nmax':10,'xTitle':'N(Ak4 jets)'}
                                                            
                    for channelDict in ChannelDictList:
                        # Dilepton #
                        plots.extend(makeSinleptonPlots(**{k:channelDict[k] for k in LeptonKeys}))
                        # Number of jets #
                        plots.append(objectsNumberPlot(**{k:channelDict[k] for k in commonItems},**JetsN))
                        # Ak4 Jets #
                        plots.extend(makeAk4JetsPlots(**{k:channelDict[k] for k in JetKeys}))
                        # MET #
                        plots.extend(makeMETPlots(**{k:channelDict[k] for k in commonItems}, met=self.corrMET))

            ##### Ak8 jets selection #####
            if "Ak8" in jetsel_level:
                print ("...... Processing Ak8 jet selection")
                ElSelObjAk8Jets = makeAk8JetSelection(self,ElSelObj,copy_sel=True,plot_yield=True)
                MuSelObjAk8Jets = makeAk8JetSelection(self,MuSelObj,copy_sel=True,plot_yield=True)
                
                # Fatjets plots #
                ChannelDictList = []
                if not self.args.OnlyYield and "Ak8" in jetplot_level:
                    ChannelDictList.append({'channel':'El','sel':ElSelObjAk8Jets.sel,'sinlepton':ElColl[0],
                                            'fatjet':self.ak8Jets[0],'suffix':ElSelObjAk8Jets.selName,'is_MC':self.is_MC})
                    ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk8Jets.sel,'sinlepton':MuColl[0],
                                            'fatjet':self.ak8Jets[0],'suffix':MuSelObjAk8Jets.selName,'is_MC':self.is_MC})

                FatJetKeys = ['channel','sel','fatjet','suffix']
                FatJetsN = {'objName':'Ak8Jets','objCont':self.ak8Jets,'Nmax':5,'xTitle':'N(Ak8 jets)'}
                
                for channelDict in ChannelDictList:
                    # Dilepton #
                    plots.extend(makeSinleptonPlots(**{k:channelDict[k] for k in LeptonKeys}))
                    # Number of jets #
                    plots.append(objectsNumberPlot(**{k:channelDict[k] for k in commonItems},**FatJetsN))
                    # Ak8 Jets #
                    plots.extend(makeAk8JetsPlots(**{k:channelDict[k] for k in FatJetKeys}))
                    # MET #
                    plots.extend(makeMETPlots(**{k:channelDict[k] for k in commonItems}, met=self.corrMET))

                         
            #-----------------------------|||||||||||||||| Resolved selection ||||||||||||||-----------------------------------#
            if any("Resolved" in item for item in jetsel_level):
                ChannelDictList = []
                # Resolved Selection (Loose) #
                #----- Resolved selection : 0 Btag -----#
                if "LooseResolved0b3j" in jetsel_level:
                    print ("...... Processing Loose Resolved jet (0 btag i.e. bTaggedJets = 0 & nLightJets >= 3) selection")
                    ElSelObjAk4JetsLooseExclusiveResolved0b3j = makeExclusiveLooseResolvedJetComboSelection(self,ElSelObjAk4JetsLoose,has0b3j=True,copy_sel=True,plot_yield=True)
                    MuSelObjAk4JetsLooseExclusiveResolved0b3j = makeExclusiveLooseResolvedJetComboSelection(self,MuSelObjAk4JetsLoose,has0b3j=True,copy_sel=True,plot_yield=True)

                    if not self.args.OnlyYield and "LooseResolved0b3j" in jetplot_level:
                        ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsLooseExclusiveResolved0b3j.sel,'sinlepton':ElColl[0],
                                                'j1':self.ak4CleanedLightJets[0],'j2':self.ak4CleanedLightJets[1],'j3':self.ak4CleanedLightJets[2],
                                                'has0b3j':True,
                                                'suffix':ElSelObjAk4JetsLooseExclusiveResolved0b3j.selName,
                                                'is_MC':self.is_MC})
                        ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsLooseExclusiveResolved0b3j.sel,'sinlepton':MuColl[0],
                                                'j1':self.ak4CleanedLightJets[0],'j2':self.ak4CleanedLightJets[1],'j3':self.ak4CleanedLightJets[2],
                                                'has0b3j':True,
                                                'suffix':MuSelObjAk4JetsLooseExclusiveResolved0b3j.selName,
                                                'is_MC':self.is_MC})


                #----  Resolved selection : 1 Btag  -----#
                if "LooseResolved1b2j" in jetsel_level:
                    print ("...... Processing Resolved jet (1 btag i.e. bTaggedJets = 1 & nLightJets >= 2) selection")
                    ElSelObjAk4JetsLooseExclusiveResolved1b2j = makeExclusiveLooseResolvedJetComboSelection(self,ElSelObjAk4JetsLoose,has1b2j=True,copy_sel=True,plot_yield=True)
                    MuSelObjAk4JetsLooseExclusiveResolved1b2j = makeExclusiveLooseResolvedJetComboSelection(self,MuSelObjAk4JetsLoose,has1b2j=True,copy_sel=True,plot_yield=True)

                    if not self.args.OnlyYield and "LooseResolved1b2j" in jetplot_level:
                        ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsLooseExclusiveResolved1b2j.sel,'sinlepton':ElColl[0],
                                                'j1':self.ak4BJets[0],'j2':self.ak4CleanedLightJets[0],'j3':self.ak4CleanedLightJets[1],
                                                'has1b2j':True,
                                                'suffix':ElSelObjAk4JetsLooseExclusiveResolved1b2j.selName,
                                                'is_MC':self.is_MC})
                        ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsLooseExclusiveResolved1b2j.sel,'sinlepton':MuColl[0],
                                                'j1':self.ak4BJets[0],'j2':self.ak4CleanedLightJets[0],'j3':self.ak4CleanedLightJets[1],
                                                'has1b2j':True,
                                                'suffix':MuSelObjAk4JetsLooseExclusiveResolved1b2j.selName,
                                                'is_MC':self.is_MC})


                #----- Resolved selection : 2 Btags -----#
                if "LooseResolved2b1j" in jetsel_level:
                    print ("...... Processing Resolved jet (2 btags i.e. bTaggedJets = 2 & nLightJets >= 1) selection")
                    ElSelObjAk4JetsLooseExclusiveResolved2b1j = makeExclusiveLooseResolvedJetComboSelection(self,ElSelObjAk4JetsLoose,has2b1j=True,copy_sel=True,plot_yield=True)
                    MuSelObjAk4JetsLooseExclusiveResolved2b1j = makeExclusiveLooseResolvedJetComboSelection(self,MuSelObjAk4JetsLoose,has2b1j=True,copy_sel=True,plot_yield=True)

                    if not self.args.OnlyYield and "LooseResolved2b1j" in jetplot_level:
                        ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsLooseExclusiveResolved2b1j.sel,'sinlepton':ElColl[0],
                                                'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJet[0],
                                                'has2b1j':True,
                                                'suffix':ElSelObjAk4JetsLooseExclusiveResolved2b1j.selName,
                                                'is_MC':self.is_MC})
                        ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsLooseExclusiveResolved2b1j.sel,'sinlepton':MuColl[0],
                                                'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJet[0],
                                                'has2b1j':True,
                                                'suffix':MuSelObjAk4JetsLooseExclusiveResolved2b1j.selName,
                                                'is_MC':self.is_MC})

                    if "LooseResolved2b2j" in jetsel_level:
                        print ("      Processing...|2-bTagged Jets(Htobb) and 2-Light Jets(Wtojj)|")    
                        ElSelObjAk4JetsExclusiveLooseResolved2b2j = makeExclusiveLooseResolvedJetComboSelection(self,
                                                                                                                ElSelObjAk4JetsLooseExclusiveResolved2b2j,
                                                                                                                has2b2j=True,copy_sel=True,plot_yield=True)
                        MuSelObjAk4JetsLooseExclusiveResolved2b2j = makeExclusiveLooseResolvedJetComboSelection(self,
                                                                                                                MuSelObjAk4JetsLooseExclusiveResolved2b2j,
                                                                                                                has2b2j=True,copy_sel=True,plot_yield=True)
                        if not self.args.OnlyYield and "LooseResolved2b2j" in jetplot_level:
                            ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsLooseExclusiveResolved2b2j.sel,'sinlepton':ElColl[0],
                                                    'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJet[0],'j4':self.ak4CleanedLightJet[1]
                                                    'has2b2j':True,
                                                    'suffix':ElSelObjAk4JetsLooseExclusiveResolved2b2j.selName,
                                                    'is_MC':self.is_MC})
                            ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsLooseExclusiveResolved2b2j.sel,'sinlepton':MuColl[0],
                                                    'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJet[0],'j4':self.ak4CleanedLightJets[1],
                                                    'has2b2j':True,
                                                    'suffix':MuSelObjAk4JetsLooseExclusiveResolved2b2j.selName,
                                                    'is_MC':self.is_MC})


                # Lepton + jet Plots #
                ResolvedBTaggedJetsN = {'objName':'Ak4BJets','objCont':self.ak4BJets,'Nmax':5,'xTitle':'N(Ak4 Bjets)'}
                ResolvedLightJetsN   = {'objName':'Ak4Jets','objCont':self.ak4CleanedLightJets,'Nmax':10,'xTitle':'N(Ak4 jets)'}
        
                for channelDict in ChannelDictList:
                    # Dilepton #
                    plots.extend(makeSinleptonPlots(**{k:channelDict[k] for k in LeptonKeys}))
                    # Number of jets #
                    plots.append(objectsNumberPlot(**{k:channelDict[k] for k in commonItems},**ResolvedBTaggedJetsN))
                    plots.append(objectsNumberPlot(**{k:channelDict[k] for k in commonItems},**ResolvedLightJetsN))
                    # Ak4 Jets #
                    plots.extend(makeAk4JetsPlots(**{k:channelDict[k] for k in JetKeys}))
                    # MET #
                    plots.extend(makeMETPlots(**{k:channelDict[k] for k in commonItems}, met=self.corrMET))
            

            
                #------------------ Resolved selection (Tight) ---------------------#
                #----- Resolved selection : 0 Btag -----#
                if "TightResolved0b4j" in jetsel_level:
                    print ("...... Processing Tight Resolved jet (0 btag i.e. bTaggedJets = 0 & nLightJets >= 4) selection")
                    ElSelObjAk4JetsTightExclusiveResolved0b3j = makeExclusiveTightResolvedJetComboSelection(self,ElSelObjAk4JetsTight,has0b4j=True,copy_sel=True,plot_yield=True)
                    MuSelObjAk4JetsTughtExclusiveResolved0b3j = makeExclusiveTightResolvedJetComboSelection(self,MuSelObjAk4JetsTight,has0b4j=True,copy_sel=True,plot_yield=True)

                    if not self.args.OnlyYield and "TightResolved0b4j" in jetplot_level:
                        ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsTightExclusiveResolved0b4j.sel,'sinlepton':ElColl[0],
                                                'j1':self.ak4CleanedLightJets[0],'j2':self.ak4CleanedLightJets[1],'j3':self.ak4CleanedLightJets[2],'j4':self.ak4CleanedLightJets[3],
                                                'has0b4j':True,
                                                'suffix':ElSelObjAk4JetsTightExclusiveResolved0b4j.selName,
                                                'is_MC':self.is_MC})
                        ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsTightExclusiveResolved0b4j.sel,'sinlepton':MuColl[0],
                                                'j1':self.ak4CleanedLightJets[0],'j2':self.ak4CleanedLightJets[1],'j3':self.ak4CleanedLightJets[2],'j4':self.ak4CleanedLightJets[3],
                                                'has0b4j':True,
                                                'suffix':MuSelObjAk4JetsTightExclusiveResolved0b4j.selName,
                                                'is_MC':self.is_MC})


                #----  Resolved selection : 1 Btag  -----#
                if "TightResolved1b3j" in jetsel_level:
                    print ("...... Processing Resolved jet (1 btag i.e. bTaggedJets = 1 & nLightJets >= 3) selection")
                    ElSelObjAk4JetsTightExclusiveResolved1b3j = makeExclusiveTightResolvedJetComboSelection(self,ElSelObjAk4JetsTight,has1b3j=True,copy_sel=True,plot_yield=True)
                    MuSelObjAk4JetsTightExclusiveResolved1b3j = makeExclusiveTightResolvedJetComboSelection(self,MuSelObjAk4JetsTight,has1b3j=True,copy_sel=True,plot_yield=True)

                    if not self.args.OnlyYield and "TightResolved1b3j" in jetplot_level:
                        ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsTightExclusiveResolved1b3j.sel,'sinlepton':ElColl[0],
                                                'j1':self.ak4BJets[0],'j2':self.ak4CleanedLightJets[0],'j3':self.ak4CleanedLightJets[1],'j4':self.ak4CleanedLightJets[2],
                                                'has1b3j':True,
                                                'suffix':ElSelObjAk4JetsTightExclusiveResolved1b3j.selName,
                                                'is_MC':self.is_MC})
                        ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsTightExclusiveResolved1b3j.sel,'sinlepton':MuColl[0],
                                                'j1':self.ak4BJets[0],'j2':self.ak4CleanedLightJets[0],'j3':self.ak4CleanedLightJets[1],'j4':self.ak4CleanedLightJets[2],
                                                'has1b2j':True,
                                                'suffix':MuSelObjAk4JetsTightExclusiveResolved1b3j.selName,
                                                'is_MC':self.is_MC})


                #----- Resolved selection : 2 Btags -----#
                if "TightResolved2b2j" in jetsel_level:
                    print ("      Processing...|2-bTagged Jets(Htobb) and 2-Light Jets(Wtojj)|")    
                    ElSelObjAk4JetsTightExclusiveResolved2b2j = makeExclusiveLooseResolvedJetComboSelection(self,ElSelObjAk4JetsTightExclusiveResolved2b2j,has2b2j=True,copy_sel=True,plot_yield=True)
                    MuSelObjAk4JetsTightExclusiveResolved2b2j = makeExclusiveLooseResolvedJetComboSelection(self,MuSelObjAk4JetsTightExclusiveResolved2b2j,has2b2j=True,copy_sel=True,plot_yield=True)
                    if not self.args.OnlyYield and "Tightesolved2b2j" in jetplot_level:
                            ChannelDictList.append({'channel':'El','sel':ElSelObjAk4JetsTightExclusiveResolved2b2j.sel,'sinlepton':ElColl[0],
                                                    'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJet[0],'j4':self.ak4CleanedLightJet[1]
                                                    'has2b2j':True,
                                                    'suffix':ElSelObjAk4JetsTightExclusiveResolved2b2j.selName,
                                                    'is_MC':self.is_MC})
                            ChannelDictList.append({'channel':'Mu','sel':MuSelObjAk4JetsTightExclusiveResolved2b2j.sel,'sinlepton':MuColl[0],
                                                    'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJet[0],'j4':self.ak4CleanedLightJets[1],
                                                    'has2b2j':True,
                                                    'suffix':MuSelObjAk4JetsTightExclusiveResolved2b2j.selName,
                                                    'is_MC':self.is_MC})

                # Lepton + jet Plots #
                ResolvedBTaggedJetsN = {'objName':'Ak4BJets','objCont':self.ak4BJets,'Nmax':5,'xTitle':'N(Ak4 Bjets)'}
                ResolvedLightJetsN   = {'objName':'Ak4LightJets','objCont':self.ak4CleanedLightJets,'Nmax':10,'xTitle':'N(Ak4 Lightjets)'}
        
                for channelDict in ChannelDictList:
                    # Dilepton #
                    plots.extend(makeSinleptonPlots(**{k:channelDict[k] for k in LeptonKeys}))
                    # Number of jets #
                    plots.append(objectsNumberPlot(**{k:channelDict[k] for k in commonItems},**ResolvedBTaggedJetsN))
                    plots.append(objectsNumberPlot(**{k:channelDict[k] for k in commonItems},**ResolvedLightJetsN))
                    # Ak4 Jets #
                    plots.extend(makeAk4JetsPlots(**{k:channelDict[k] for k in JetKeys}))
                    # MET #
                    plots.extend(makeMETPlots(**{k:channelDict[k] for k in commonItems}, met=self.corrMET))


            #----- High-level combinations -----#
            if not self.args.OnlyYield:
                ChannelDictList = []
                # LooseResolved
                # Resolved No Btag #
                if "LooseResolved0b3j" in jetplot_level:
                    ChannelDictList.append({'channel': 'El','met': self.corrMET,'l1':ElColl[0],
                                            'j1':self.ak4CleanedLightJets[0],'j2':self.ak4CleanedLightJets[1],'j3':self.ak4CleanedLightJets[2],
                                            'has0b3j':True,'sel':ElSelObjAk4JetsLooseExclusiveResolved0b3j.sel,
                                            'suffix':ElSelObjAk4JetsLooseExclusiveResolved0b3j.selName})
                    ChannelDictList.append({'channel': 'Mu','met': self.corrMET,'l1':MuColl[0],
                                            'j1':self.ak4CleanedLightJets[0],'j2':self.ak4CleanedLightJets[1],'j3':self.ak4CleanedLightJets[2],
                                            'has0b3j':True,'sel':MuSelObjAk4JetsLooseExclusiveResolved0b3j.sel,
                                            'suffix':MuSelObjAk4JetsLooseExclusiveResolved0b3j.selName})
                # Resolved One Btag Two jet #
                if "LooseResolved1b2j" in jetplot_level:
                    ChannelDictList.append({'channel': 'El','met': self.corrMET,'l1':ElColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4CleanedLightJets[0],'j3':self.ak4CleanedLightJets[1],
                                            'has1b2j':True,'sel':ElSelObjeAk4JetsLooseExclusiveResolved1b2j.sel,
                                            'suffix':ElSelObjAk4JetsLooseExclusiveResolved1b2j.selName})
                    ChannelDictList.append({'channel': 'Mu','met': self.corrMET,'l1':MuColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4CleanedLightJets[0],'j3':self.ak4CleanedLightJets[1],
                                            'has1b2j':True,'sel':MuSelObjeAk4JetsLooseExclusiveResolved1b2j.sel,
                                            'suffix':MuSelObjAk4JetsLooseExclusiveResolved1b2j.selName})
                # Resolved Two Btag One jet #      
                if "LooseResolved2b1j" in jetplot_level:
                    ChannelDictList.append({'channel': 'El','met': self.corrMET,'l1':ElColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJets[0],
                                            'has2b1j':True,'sel':ElSelObjAk4JetsLooseExclusiveResolved2b1j.sel,
                                            'suffix':ElSelObjAk4JetsLooseExclusiveResolved2b1j.selName})
                    ChannelDictList.append({'channel': 'Mu','met': self.corrMET,'l1':MuColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJets[0],
                                            'has2b1j':True,'sel':MuSelObjAk4JetsLooseExclusiveResolved2b1j.sel,
                                            'suffix':MuSelObjAk4JetsLooseExclusiveResolved2b1j.selName})
                # Resolved Two Btag Two jet #      
                if "LooseResolved2b2j" in jetplot_level:
                    ChannelDictList.append({'channel': 'El','met': self.corrMET,'l1':ElColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJets[0],'j4':self.ak4CleanedLightJets[1],
                                            'has2b2j':True,'sel':ElSelObjAk4JetsLooseExclusiveResolved2b2j.sel,
                                            'suffix':ElSelObjAk4JetsLooseExclusiveResolved2b2j.selName})
                    ChannelDictList.append({'channel': 'Mu','met': self.corrMET,'l1':MuColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJets[0],'j4':self.ak4CleanedLightJets[1],
                                            'has2b2j':True,'sel':MuSelObjAk4JetsLooseExclusiveResolved2b2j.sel,
                                            'suffix':MuSelObjAk4JetsLooseExclusiveResolved2b2j.selName})

                # TightResolved    
                # Resolved No Btag #
                if "TightResolved0b4j" in jetplot_level:
                    ChannelDictList.append({'channel': 'El','met': self.corrMET,'l1':ElColl[0],
                                            'j1':self.ak4CleanedLightJets[0],'j2':self.ak4CleanedLightJets[1],'j3':self.ak4CleanedLightJets[2],'j4':self.ak4CleanedLightJets[3],
                                            'has0b4j':True,'sel':ElSelObjAk4JetsTightExclusiveResolved0b4j.sel,
                                            'suffix':ElSelObjAk4JetsTightExclusiveResolved0b4j.selName})
                    ChannelDictList.append({'channel': 'Mu','met': self.corrMET,'l1':MuColl[0],
                                            'j1':self.ak4CleanedLightJets[0],'j2':self.ak4CleanedLightJets[1],'j3':self.ak4CleanedLightJets[2],'j4':self.ak4CleanedLightJets[3],
                                            'has0b4j':True,'sel':MuSelObjAk4JetsTightExclusiveResolved0b4j.sel,
                                            'suffix':MuSelObjAk4JetsTightExclusiveResolved0b4j.selName})
                # Resolved One Btag Three jet #
                if "TightResolved1b3j" in jetplot_level:
                    ChannelDictList.append({'channel': 'El','met': self.corrMET,'l1':ElColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4CleanedLightJets[0],'j3':self.ak4CleanedLightJets[1],'j4':self.ak4CleanedLightJets[2],
                                            'has1b3j':True,'sel':ElSelObjeAk4JetsTightExclusiveResolved1b3j.sel,
                                            'suffix':ElSelObjAk4JetsTightExclusiveResolved1b3j.selName})
                    ChannelDictList.append({'channel': 'Mu','met': self.corrMET,'l1':MuColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4CleanedLightJets[0],'j3':self.ak4CleanedLightJets[1],'j4':self.ak4CleanedLightJets[2],
                                            'has1b2j':True,'sel':MuSelObjeAk4JetsTightExclusiveResolved1b3j.sel,
                                            'suffix':MuSelObjAk4JetsTightExclusiveResolved1b3j.selName})
                # Resolved Two Btag Two jet #      
                if "TightResolved2b2j" in jetplot_level:
                    ChannelDictList.append({'channel': 'El','met': self.corrMET,'l1':ElColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJets[0],'j4':self.ak4CleanedLightJets[1],
                                            'has2b2j':True,'sel':ElSelObjAk4JetsTightExclusiveResolved2b2j.sel,
                                            'suffix':ElSelObjAk4JetsTightExclusiveResolved2b2j.selName})
                    ChannelDictList.append({'channel': 'Mu','met': self.corrMET,'l1':MuColl[0],
                                            'j1':self.ak4BJets[0],'j2':self.ak4BJets[1],'j3':self.ak4CleanedLightJets[0],'j4':self.ak4CleanedLightJets[1],
                                            'has2b2j':True,'sel':MuSelObjAk4JetsTightExclusiveResolved2b2j.sel,
                                            'suffix':MuSelObjAk4JetsTightExclusiveResolved2b2j.selName})                 

                for channelDict in ChannelDictList:
                    plots.extend(makeHighLevelQuantities(**channelDict))

            

            if any("SemiBoosted" in item for item in jetsel_level):
                ChannelDictList=[]
                # Semi-Boosted (Htobb:Ak8 + Wtojj:Ak4) 
                if "SemiBoostedHfatWslim" in jetsel_level:
                    print ("...... Processing Semi-Boosted category (Htobb:Ak8 + Wtojj:Ak4) selection :: nAk8-bJets >= 1 + nAk4Jets[dR(ak8b,ak4) > 0.8] >= 1")
                

                if "SemiBoostedHslimWfat" in jetsel_level:
                    print ("...... Processing Semi-Boosted category (Htobb:Ak4 + Wtojj:Ak8) selection :: nAk8-bJets >= 1 + nAk4Jets[dR(ak8b,ak4) > 0.8] >= 1")    





            if "Boosted" in jetsel_level:
                print ("...... Processing Boosted category selection")        






        #----- Add the Yield plots -----#
        plots.extend(self.yieldPlots.returnPlots())

        return plots

