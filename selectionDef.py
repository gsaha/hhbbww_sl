import os
import sys
from copy import copy
from bamboo import treefunctions as op

#===============================================================================================#
#                                  SelectionObject class                                        #
#===============================================================================================#
class SelectionObject:
    """
        Helper class to hold a selection including :
        sel         : refine object
        selName     : name of the selection used in the refine
        yieldTitle  : Title for the yield table in LateX
        NB : copy() might be useful to be able to branch out the selection
    """
    def __init__(self,sel,selName,yieldTitle):
        self.sel        = sel
        self.selName    = selName
        self.yieldTitle = yieldTitle
    
    def refine(self,cut=None,weight=None,autoSyst=True):
        """
        Overload the refine function to avoid repeating the selName arg 
        """
        self.sel = self.sel.refine(name    = self.selName,
                                   cut     = cut,
                                   weight  = weight,
                                   autoSyst= autoSyst)
    def makeYield(self,yieldObject):
        """
        Record an entry in the yield table 
        """
        yieldObject.addYield(sel    = self.sel,
                             name   = self.selName,
                             title  = self.yieldTitle)

#===============================================================================================#
#                                        Selections                                             #
#===============================================================================================#

def makeLeptonSelection(self,baseSel,plot_yield=False): 
    """
    Produces the requested lepton selection (encapsulated in SelectionObject class objects)
    Will produce a dict :
        - key = level required (Preselected, Fakeable, Tight and/or FakeExtrapolation)
        - value = list of SelectionObject class objects per channel [ElEl,MuMu,ElMu] 
    We start by leptons so no need to pass selObject
    Code organized such that selections are not repeated and hopefully optimzed the RooDataFrame
    """
    # Select level #
    sel_level = []   # What lepton type selection to perform (e.g. Tight needs Fakeable and Preselected)
    lepton_level = ["Preselected","Fakeable","Tight","FakeExtrapolation"]
    # What lepton type selection to save for plotting
    save_level = [arg for (arg,boolean) in self.args.__dict__.items() if arg in lepton_level and boolean]  
        # Make list of requested lepton selection in the arguments
    if len(save_level) == 0: save_level = lepton_level # If nothing asked, all will be done

    if "Tight" in save_level:
        sel_level.extend(["Preselected","Fakeable","Tight"]) # Tight needs Fakeable and Preselected
    if "FakeExtrapolation" in save_level:
        sel_level.extend(["Preselected","Fakeable","FakeExtrapolation"]) # FakeExtrapolation needs Fakeable and Preselected
    if "Fakeable" in save_level:
        sel_level.extend(["Preselected","Fakeable"]) # Fakeable needs Preselected
    if "Preselected" in save_level:
        sel_level.extend(["Preselected"]) 
            # TODO : find cleaner-> if args.Tight And args.FakeExtrapolation : will be redundancies (not a problem though)
    #--- Lambdas ---#
    # Fakeable lambdas #
    lambdaLowPtCut = lambda lepColl: lepColl[0].pt > 15 # subleading above 15 GeV
    lambdaLeadingPtCut = lambda lepColl: lepColl[0].pt > 25 # leading above 25 GeV

    ElLooseSF = lambda lepColl : [self.lambda_ttH_doubleElectron_trigSF(lepColl)] + \
                                 self.lambda_ElectronLooseSF(lepColl[0]) \
                                 if self.is_MC else None
    MuLooseSF = lambda lepColl : [self.lambda_ttH_doubleMuon_trigSF(lepColl)] + \
                                 self.lambda_MuonLooseSF(lepColl[0]) \
                                 if self.is_MC else None
    
    # Tight SF lambdas #
    ElTightSF = lambda dilep : self.lambda_ElectronTightSF(dilep[0])+self.lambda_ElectronTightSF(dilep[1]) if self.is_MC else None
    MuTightSF = lambda dilep : self.lambda_MuonTightSF(dilep[0])+self.lambda_MuonTightSF(dilep[1]) if self.is_MC else None
    
    #--- Preselection ---#
    selectionDict = {}

    if "Preselected" in sel_level:
        ElPreSelObject = SelectionObject(sel          = baseSel,
                                         selName      = "HasElPreselected",
                                         yieldTitle   = "Preselected lepton (channel $e^+/e^-$)")
        MuPreSelObject = SelectionObject(sel          = baseSel,
                                         selName      = "HasMuPreselected",
                                         yieldTitle   = "Preselected lepton (channel $\mu^+/\mu^-$)")

        # Selection #
        ElPreSelObject.refine(cut     = [op.rng_len(self.electronsPreSel) == 1],
                              weight  = ElLooseSF(self.electronPreSel[0]))
        MuPreSelObject.refine(cut     = [op.rng_len(self.muonPreSel) == 1],
                              weight  = MuMuLooseSF(self.muonPreSel[0]))

        # Yield #
        if plot_yield:
            ElPreSelObject.makeYield(self.yieldPlots)
            MuPreSelObject.makeYield(self.yieldPlots)
            
        # Record if actual argument #
        if "Preselected" in save_level:
            selectionDict["Preselected"] = [ElPreSelObject,MuPreSelObject]

        #--- Fakeable ---#
        if "Fakeable" in sel_level:
            ElFakeSelObject = SelectionObject(sel         = ElPreSelObject.sel,
                                              selName     = "HasElFakeable",
                                              yieldTitle  = "Fakeable lepton (channel $e^+/e^-$)")
            MuFakeSelObject = SelectionObject(sel         = MuPreSelObject.sel,
                                              selName     = "HasMuFakeable",
                                              yieldTitle  = "Fakeable lepton (channel $\mu^+/\mu^-$)")

            # Selection : at least one fakeable dilepton #
            ElFakeSelObject.refine(cut = [op.rng_len(self.electronFakeSel) == 1,lambdaLowPtCut(self.electronFakeSel[0])])
            MuFakeSelObject.refine(cut = [op.rng_len(self.muonFakeSel) == 1,lambdaLowPtCut(self.muonFakeSel[0])])
            
            # Yield #
            if plot_yield:
                ElFakeSelObject.makeYield(self.yieldPlots)
                MuFakeSelObject.makeYield(self.yieldPlots)
                                    
            # Record if actual argument #
            if "Fakeable" in save_level:
                selectionDict["Fakeable"] = [ElFakeSelObject,MuFakeSelObject]
                
            #--- Tight ---#
            if "Tight" in sel_level:
                ElTightSelObject = SelectionObject(sel          = ElFakeSelObject.sel,
                                                   selName      = "HasElTight",
                                                   yieldTitle   = "Tight lepton (channel $e^+/e^-$)")
                MuTightSelObject = SelectionObject(sel          = MuFakeSelObject.sel,
                                                   selName      = "HasMuTight",
                                                   yieldTitle   = "Tight lepton (channel $\mu^+/\mu^-$)")
                
                # Selection : at least one tight dilepton #
                ElTightSelObject.refine(cut    = [op.rng_len(self.electronTightSel) == 1],
                                        weight = ElTightSF(self.electronTightSel[0]))
                MuTightSelObject.refine(cut    = [op.rng_len(self.muonTightSel) == 1],
                                        weight = MuTightSF(self.muonTightSel[0]))

                # Yield #
                if plot_yield:
                    ElTightSelObject.makeYield(self.yieldPlots)
                    MuTightSelObject.makeYield(self.yieldPlots)
                
                # Record if actual argument #
                if "Tight" in save_level:
                    selectionDict["Tight"] = [ElTightSelObject,MuTightSelObject]

            #--- Fake extrapolated ---#
            if "FakeExtrapolation" in sel_level:
                ElFakeExtrapolationSelObject = SelectionObject(sel          = ElFakeSelObject.sel,
                                                               selName      = "HasElFakeExtrapolation",
                                                               yieldTitle   = "Fake extrapolated lepton (channel $e^+/e^-$)")
                MuFakeExtrapolationSelObject = SelectionObject(sel          = MuFakeSelObject.sel,
                                                               selName      = "HasMuFakeExtrapolation",
                                                               yieldTitle   = "Fake extrapolated lepton (channel $\mu^+/\mu^-$)")
                
                # Selection : at least one tight dilepton #
                ElFakeExtrapolationSelObject.refine(cut    = [op.rng_len(self.electronTightSel) == 0, op.rng_len(self.electronFakeExtrapolationSel) >= 1],
                                                    weight = ElTightSF(self.electronFakeExtrapolationSel[0])) # TODO : should I ?

                MuFakeExtrapolationSelObject.refine(cut    = [op.rng_len(self.muonTightSel) == 0, op.rng_len(self.muonFakeExtrapolationSel) >= 1],
                                                    weight = MuTightSF(self.muonFakeExtrapolationSel[0])) # TODO : should I ?
                
                # Yield #
                if plot_yield:
                    ElFakeExtrapolationSelObject.makeYield(self.yieldPlots)
                    MuFakeExtrapolationSelObject.makeYield(self.yieldPlots)
                    
                # Record if actual argument #
                if "FakeExtrapolation" in save_level:
                    selectionDict["FakeExtrapolation"] = [ElFakeExtrapolationSelObject,MuFakeExtrapolationSelObject]
    # Return # 
    return selectionDict

def makeAk4JetSelection(self,selObject,nJet,copy_sel=False,plot_yield=False):
    """
    Produces the Ak4 jet selection
    inputs :
        - selObject : SelectionObject class objects (contains bamboo selection, its name and the yield title)
        - copy_sel : wether to copy and return a new selection object built on the one provided, if not will modify it
    Careful : if copy_sel is False, the selObject will be modified
    Selection : at least two Ak4 jets
    """

    if copy_sel:
        selObject = copy(selObject)
    if nJet=3: 
        selObject.selName += "Ak4JetsLoose"
        selObject.yieldTitle += " + Ak4 Jets Loose $\geq 2$"
    elif nJet=4:
        selObject.selName += "Ak4JetsTight"
        selObject.yieldTitle += " + Ak4 Jets Tight $\geq 2$"
    selObject.sel = selObject.sel.refine(selObject.selName,cut=[op.rng_len(self.ak4Jets)>=nJet])
    if plot_yield:
        selObject.makeYield(self.yieldPlots)
    if copy_sel :
        return selObject 


def makeAk8JetSelection(self,selObject,copy_sel=False,plot_yield=False):
    """
    Produces the Ak8 jet selection
    inputs :
        - selObject : SelectionObject class objects (contains bamboo selection, its name and the yield title)
        - copy_sel : wether to copy and return a new selection object built on the one provided, if not will modify it
    Careful : if copy_sel is False, the selObject will be modified
    Selection : at least one Ak8 jet
    """
    if copy_sel:
        selObject = copy(selObject)
    selObject.selName += "OneAk8Jet"
    selObject.yieldTitle += " + Ak8 Jets $\geq 1$"
    selObject.refine(cut=[op.rng_len(self.ak8Jets)>=1])
    if plot_yield:
        selObject.makeYield(self.yieldPlots)
    if copy_sel:
        return selObject
    

def makeExclusiveLooseResolvedJetComboSelection(self,selObject,
                                                has0b3j=False,has1b2j=False,has2b1j=False,has2b2j=False,
                                                copy_sel=False,plot_yield=False):
    if copy_sel:
        selObject = copy(selObject)
    selObject.refine(cut=[op.rng_len(self.ak8Jets)==0], weight=None)

    if has0b3j:
        AppliedSF = None
        #----- DY estimation from data -----#
        if self.args.DYDataEstimation1Btag or self.args.DYDataEstimation2Btag:
            if self.is_MC:
                print ("Warning : the DY reweighting is not applied on MC, will ignore that step")
            else:
                if "ElEl" in selObject.selName:
                    if self.DYReweightingElEl is None:
                        raise RuntimeError('DY reweighting for ElEl is not initialized')
                    AppliedDYReweighting = self.DYReweightingElEl(self.OSElElDileptonTightSel[0][0]) # Only on highest PT electron
                elif "MuMu" in selObject.selName:
                    if self.DYReweightingMuMu is None:
                        raise RuntimeError('DY reweighting for MuMu is not initialized')
                    AppliedDYReweighting = self.DYReweightingMuMu(self.OSMuMuDileptonTightSel[0][0]) # Only on highest PT muon
                else: # No DY reweighting in ElMu (marginal contribution)
                    AppliedDYReweighting = None
                AppliedSF = [AppliedDYReweighting] if AppliedDYReweighting is not None else None
                # weight = None works, but not weight = [None]
        selObject.selName += "ExclusiveResolved0b3j"
        selObject.yieldTitle += " + Exclusive Resolved (0 bjet, 3 lightJets)"
        selObject.refine(cut=[op.rng_len(self.ak4BJets)==0, op.rng_len(self.ak4CleanedLightJets)>=3],
                         weight=AppliedSF)
        if plot_yield:
            selObject.makeYield(self.yieldPlots)

    elif has1b2j:
        AppliedSF = [self.DeepJetMediumSF(self.ak4BJets[0])] if self.is_MC else None
        selObject.selName += "ExclusiveResolvedOneBtag"
        selObject.yieldTitle += " + Exclusive Resolved (1 bjet)"
        #exclusive 1b-Jet Selection (nAk4bJets=1 & nAk8bJets=0)
        #Applied the SFs as weight
        selObject.refine(cut    = [op.rng_len(self.ak4BJets)==1, op.rng_len(self.ak4CleanedLightJets)>=2],
                         weight = AppliedSF)
        if plot_yield:
            selObject.makeYield(self.yieldPlots)

    elif has2b1j or has2b2j:
        AppliedSF = [self.DeepJetMediumSF(self.ak4BJets[0]),self.DeepJetMediumSF(self.ak4BJets[1])] if self.is_MC else None
        selObject.selName += "ExclusiveResolvedTwoBtags"
        selObject.yieldTitle += " + Exclusive Resolved (2 bjets)"
        selObject.refine(cut    = [op.rng_len(self.ak4BJets)==2],
                         weight = AppliedSF)
        if self.args.TTBarCR:
            selObject.selName += "MbbCut150"
            selObject.yieldTitle += " + $M_{bb}>150$"
            selObject.refine(cut = [op.invariant_mass(self.ak4BJets[0].p4,self.ak4BJets[1].p4)>150])
            if plot_yield:
                selObject.makeYield(self.yieldPlots)


        if has2b1j:
            selObject.refine(cut    = [op.rng_len(self.ak4CleanedLightJets)>=1], weight=None)
            if plot_yield:
                selObject.makeYield(self.yieldPlots)

        elif has2b2j:
            selObject.refine(cut    = [op.rng_len(self.ak4CleanedLightJets)>=2], weight=None)
            if plot_yield:
                selObject.makeYield(self.yieldPlots)

    if copy_sel:
        return selObject


def makeExclusiveTightResolvedJetComboSelection(self,selObject,
                                                has0b4j=False,has1b3j=False,has2b2j=False,
                                                copy_sel=False,plot_yield=False):
    if copy_sel:
        selObject = copy(selObject)
    selObject.refine(cut=[op.rng_len(self.ak8Jets)==0], weight=None)

    if has0b4j:
        AppliedSF = None
        #----- DY estimation from data -----#
        if self.args.DYDataEstimation1Btag or self.args.DYDataEstimation2Btag:
            if self.is_MC:
                print ("Warning : the DY reweighting is not applied on MC, will ignore that step")
            else:
                if "ElEl" in selObject.selName:
                    if self.DYReweightingElEl is None:
                        raise RuntimeError('DY reweighting for ElEl is not initialized')
                    AppliedDYReweighting = self.DYReweightingElEl(self.OSElElDileptonTightSel[0][0]) # Only on highest PT electron
                elif "MuMu" in selObject.selName:
                    if self.DYReweightingMuMu is None:
                        raise RuntimeError('DY reweighting for MuMu is not initialized')
                    AppliedDYReweighting = self.DYReweightingMuMu(self.OSMuMuDileptonTightSel[0][0]) # Only on highest PT muon
                else: # No DY reweighting in ElMu (marginal contribution)
                    AppliedDYReweighting = None
                AppliedSF = [AppliedDYReweighting] if AppliedDYReweighting is not None else None
                # weight = None works, but not weight = [None]
        selObject.selName += "ExclusiveResolvedNoBtag"
        selObject.yieldTitle += " + Exclusive Resolved (0 bjet)"
        selObject.refine(cut=[op.rng_len(self.ak4BJets)==0, op.rng_len(self.ak4CleanedLightJets)>=4],
                        weight=AppliedSF)
        if plot_yield:
            selObject.makeYield(self.yieldPlots)

    elif has1b2j:
        AppliedSF = [self.DeepJetMediumSF(self.ak4BJets[0])] if self.is_MC else None
        selObject.selName += "ExclusiveResolvedOneBtag"
        selObject.yieldTitle += " + Exclusive Resolved (1 bjet)"
        #exclusive 1b-Jet Selection (nAk4bJets=1 & nAk8bJets=0)
        #Applied the SFs as weight
        selObject.refine(cut    = [op.rng_len(self.ak4BJets)==1, op.rng_len(self.ak4CleanedLightJets)>=3],
                         weight = AppliedSF)
        if plot_yield:
            selObject.makeYield(self.yieldPlots)

    elif has2b2j:
        AppliedSF = [self.DeepJetMediumSF(self.ak4BJets[0]),self.DeepJetMediumSF(self.ak4BJets[1])] if self.is_MC else None
        selObject.selName += "ExclusiveResolvedTwoBtags"
        selObject.yieldTitle += " + Exclusive Resolved (2 bjets)"
        selObject.refine(cut    = [op.rng_len(self.ak4BJets)==2],
                         weight = AppliedSF)
        if self.args.TTBarCR:
            selObject.selName += "MbbCut150"
            selObject.yieldTitle += " + $M_{bb}>150$"
            selObject.refine(cut = [op.invariant_mass(self.ak4BJets[0].p4,self.ak4BJets[1].p4)>150])
            if plot_yield:
                selObject.makeYield(self.yieldPlots)

        selObject.refine(cut    = [op.rng_len(self.ak4CleanedLightJets)>=2], weight=None)
        if plot_yield:
            selObject.makeYield(self.yieldPlots)

    if copy_sel:
        return selObject


def makeInclusiveBoostedSelection(self,selObject,copy_sel=False,plot_yield=False):
    """
    Produces the inclusive boosted selection
    inputs :
        - selObject : SelectionObject class objects (contains bamboo selection, its name and the yield title)
        - copy_sel : wether to copy and return a new selection object built on the one provided, if not will modify it
    Careful : if copy_sel is False, the selObject will be modified
    Selection : At least one btagged Ak8 jet
    """
    if copy_sel:
        selObject = copy(selObject)
    AppliedSF = None # TODO: correct at v7
    selObject.selName += "InclusiveBoosted"
    selObject.yieldTitle += " + Inclusive Boosted"
    selObject.refine(cut    = [op.rng_len(self.ak8BJets)==1],
                     weight = AppliedSF)
    if plot_yield:
        selObject.makeYield(self.yieldPlots)
    if copy_sel:
        return selObject
